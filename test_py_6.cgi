#!/usr/bin/env python
import cgitb
from fractions import gcd
import pymysql
import cgi

cgitb.enable()
form=cgi.FieldStorage()

full_name_1 = form.getvalue('del_name')

#create and pymysql object and assign the credentials
my_con=pymysql.connect(database='621', user='root', password='root', host='127.0.0.1', port=8889)
c=my_con.cursor()

#delete data from database
c.execute("DELETE FROM student_grades WHERE full_name=%s",
    (full_name_1))

#commit anytime you make change
my_con.commit()

print "Content-type: text/html\n\n";
print("<html>")
print("<head>")
print("<title>Exercise 4</title>")
print("</head>")
print("<body>")
print("Deleted Grades")
print("</body>")
print("</html>")

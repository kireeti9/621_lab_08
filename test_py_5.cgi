#!/usr/bin/env python
import cgitb
from fractions import gcd
import pymysql
import cgi

cgitb.enable()
form=cgi.FieldStorage()

#create and pymysql object and assign the credentials
my_con=pymysql.connect(database='621', user='root', password='root', host='127.0.0.1', port=8889)
c=my_con.cursor()

#insert data into database
c.execute("SELECT * FROM student_grades")

#commit anytime you make change
my_con.commit()

results = c.fetchall()

print "Content-type: text/html\n\n";
print("<html>")
print("<head>")
print("<title>Exercise 4</title>")
print("</head>")
print("<body>")
print("<div>")
print("<table>")
print("<thead>")
print("<tr>")
print("<th>full_name</th>")
print("<th>mid1</th>")
print("<th>mid2</th>")
print("<th>final</th>")
print("<th>Avg</th>")
print("</tr>")
print("</thead>")
print("<tbody>")

for row in results:
    full_name = row[1]
    mid1 = row[2]
    mid2 = row[3]
    final = row[4]
    avg = int(mid1) + int(mid2) + int(final)

    print("<tr>")
    print("<td>")
    print(full_name)
    print("</td>")
    print("<td>")
    print(mid1)
    print("</td>")
    print("<td>")
    print(mid2)
    print("</td>")
    print("<td>")
    print(final)
    print("</td>")
    print("<td>")
    print(avg/3)
    print("</td>")
    print("</tr>")
print("</tbody>")
print("</table>")
print("</div>")
print("</body>")
print("</html>")


















#!/usr/bin/env python
import cgitb
from fractions import gcd
import pymysql
import cgi

cgitb.enable()
form=cgi.FieldStorage()

full_name_1 = form.getvalue('full_name')
mid_11 = int(form.getvalue('mid_1'))
mid_21 = int(form.getvalue('mid_2'))
final1 = int(form.getvalue('final'))

#create and pymysql object and assign the credentials
my_con=pymysql.connect(database='621', user='root', password='root', host='127.0.0.1', port=8889)
c=my_con.cursor()

#insert data into database
c.execute("INSERT INTO student_grades (full_name, mid_1, mid_2, final) VALUES (%s, %s, %s, %s)",
    (full_name_1, mid_11, mid_21, final1))

#commit anytime you make change
my_con.commit()

print "Content-type: text/html\n\n";
print("<html>")
print("<head>")
print("<title>Exercise 4</title>")
print("</head>")
print("<body>")
print("Added Grades")
print("</body>")
print("</html>")
